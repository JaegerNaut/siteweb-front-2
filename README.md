# siteweb-front-2

> My phenomenal Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

Menu façon responsive qui claque : [lien](https://codepen.io/iamturner/pen/RaqoPX)

Sidebar pour le menu administration : [lien](https://codepen.io/maggiben/pen/rCIFu)
