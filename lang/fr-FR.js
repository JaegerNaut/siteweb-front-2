export default {
  welcome: 'welcome',
  links: {
    about: '/a-propos',
    project: '/projet',
    legalNotice: '/mentions-legales'
  },
  navbar: {
    home: 'Accueil',
    about: 'À propos',
    projects: 'Projets',
    contact: 'Contactez-moi'
  },
  article: {
    read: 'Lire',
    load: 'Afficher plus d\'articles',
    no_more: 'Tous les articles sont affichés',
    comment: 'commentaire | commentaire | commentaires',
    reply: 'Répondre',
    related: 'Related articles'
  },
  comment: {
    email_p: 'Email (ne sera pas affiché)',
    username: 'Pseudo',
    send: 'Envoyer',
    cancel: 'Annuler',
    ok: 'Merci ! Votre commentaire apparaitra une fois vérifié !'
  },
  tag: {
    project: 'Projets liés à',
    article: 'Articles liés à',
    all: 'Contenu lié à'
  },
  project: {
    simple: 'projet',
    clean: 'Projet',
    see: 'Voir plus',
    github: 'Voir le dépôt',
    website: 'Voir le site',
    related: 'Projets similaires'
  },
  contact: {
    intro: 'Besoin de me contacter ? Aucun souci, envoyez-moi un mail à %mail% ou utilisez cet incroyable formulaire !',
    email: 'Adresse e-mail',
    name: 'Nom',
    subject: 'Objet',
    send: 'Envoyer',
    ok: 'Merci ! Je vous recontacte dès que j\'aurai lu votre message !',
    missing: 'Le champ "%field%" est requis',
    fakeMail: 'Veuillez saisir un email valide'
  },
  misc: {
    years: 'ans',
    all: 'Tout',
    back: 'Retour',
    denied: 'Vous n\'êtes pas autorisé à voir ce contenu.',
    legalNotice: 'Mentions légales',
    cookies: 'Ce site utilise des cookies pour fonctionner correctement. Vous pouvez voir leur utilisation',
    cookieLink: 'en suivant ce lien',
    cookieClose: '(Cliquez sur la bannière pour la fermer)'
  }
}
