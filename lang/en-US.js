export default {
  welcome: 'welcome',
  links: {
    about: '/en/about',
    project: '/en/project',
    legalNotice: '/en/legal-notice'
  },
  navbar: {
    home: 'Home',
    about: 'About',
    projects: 'Projects',
    contact: 'Contact me'
  },
  article: {
    read: 'Read more',
    load: 'Load more articles',
    no_more: 'All article displayed',
    comment: 'comment | comment | comments',
    reply: 'Reply',
    related: 'Related articles'
  },
  comment: {
    email_p: 'Email (won\'t be displayed)',
    username: 'Username',
    send: 'Send',
    cancel: 'Cancel',
    ok: 'Thanks! Your comment will show up once reviewed!'
  },
  tag: {
    project: 'Related projects for',
    article: 'Related articles for',
    all: 'Related content for'
  },
  project: {
    simple: 'project',
    clean: 'Project',
    see: 'See more',
    github: 'Check the repository',
    website: 'See the website',
    related: 'Related projects'
  },
  contact: {
    intro: 'Need to get in touch? No problem, just send me a mail to %mail%, or use this incredible form right there',
    email: 'Email address',
    name: 'Name',
    subject: 'Subject',
    send: 'Send',
    ok: 'Gotcha, I\'ll get back to you once I read your mail!',
    missing: 'The field "%field%" is required',
    fakeMail: 'Please enter a valid email'
  },
  misc: {
    years: 'years old',
    all: 'All',
    back: 'Back',
    denied: 'You are not allowed to reach this content.',
    legalNotice: 'Legal notice',
    cookies: 'This websites uses cookies to run properly. You can see their purpose',
    cookieLink: 'by clicking here',
    cookieClose: '(Click the banner to close it)'
  }
}
